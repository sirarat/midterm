package com.sirarat.shopmt;

import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author admin02
 */
public class Product implements Serializable{

    private String ID;
    private String Name;
    private String Brand;
    private Double Price;
    private int amount;

    public Product(String ID, String Name, String Brand, Double Price,int amount) {
        this.ID = ID;
        this.Name = Name;
        this.Brand = Brand;
        this.Price = Price;
    }

  

   

    @Override
    public String toString() {
        return   ID +"                       " + Name + "                     " + Brand + "                               " + Price + "                                         " + amount ;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String Brand) {
        this.Brand = Brand;
    }

    public Double getPrice() {
        return Price;
    }

    public void setPrice(Double Price) {
        this.Price = Price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

}
